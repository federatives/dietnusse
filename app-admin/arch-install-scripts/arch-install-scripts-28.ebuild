# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Useful installation scripts ported from Arch Linux"
HOMEPAGE="https://github.com/archlinux/arch-install-scripts"

SRC_URI="https://github.com/archlinux/arch-install-scripts/archive/refs/tags/v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"

KEYWORDS="~amd64 ~x86 ~alpha ~arm ~arm64 ~hppa ~ia64 ~ppc ~ppc64 ~riscv ~sparc"

RDEPEND="app-alternatives/awk
	 app-shells/bash
	 sys-apps/coreutils
	 sys-apps/grep
	 sys-apps/util-linux"
DEPEND="${RDEPEND}"
BDEPEND="app-text/asciidoc
	 app-arch/gzip"

src_install() {
	emake PREFIX=/usr DESTDIR="${D}" install
	# Since pacman is not available, remove pacstrap and its stuff
	rm ${D}/usr/bin/pacstrap
	rm ${D}/usr/share/man/man8/pacstrap.8
	rm ${D}/usr/share/bash-completion/completions/pacstrap
}
