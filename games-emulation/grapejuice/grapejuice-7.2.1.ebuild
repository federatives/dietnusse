# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{8..11} )
inherit python-single-r1

DESCRIPTION="A Wine+Roblox management tool"
HOMEPAGE="https://gitlab.com/brinkervii/grapejuice"

SRC_URI="https://gitlab.com/brinkervii/grapejuice/-/archive/v${PV}/grapejuice-v${PV}.zip"
S="${WORKDIR}/grapejuice-v${PV}"

LICENSE="GPL-3"
SLOT="0"

KEYWORDS="-* ~amd64"

IUSE="pulseaudio"
RDEPEND="sys-devel/gettext
	 net-libs/gnutls[abi_x86_32]
	 pulseaudio? ( media-libs/libpulse[abi_x86_32] )
	 >=x11-libs/gtk+-3.24.34
	 x11-libs/cairo
	 x11-misc/xdg-utils
	 x11-misc/xdg-user-dirs
	 x11-misc/shared-mime-info
	 x11-apps/mesa-progs
	 dev-util/gtk-update-icon-cache
	 dev-util/desktop-file-utils
	 dev-libs/openssl[abi_x86_32]
	 dev-libs/gobject-introspection
	 dev-python/pip
	 dev-python/psutil
	 dev-python/click
	 dev-python/requests
	 dev-python/unidecode
	 dev-python/pygobject
	 dev-python/packaging
	 dev-python/setuptools
	 virtual/wine"
DEPEND="${RDEPEND}"
BDEPEND="app-arch/unzip
	 app-arch/tar"

src_compile() {
	export PYTHONPATH="${S}/src"
	${EPYTHON} -m grapejuice_packaging linux_package || die
}

src_install() {
	tar -xvf ${S}/dist/linux_package/*.tar.gz -C "${D}"
	mkdir -p "${D}/usr/lib/${EPYTHON}"
	mv "${D}/usr/lib/python3/dist-packages" "${D}/usr/lib/${EPYTHON}/site-packages"
	rm -r "${D}/usr/lib/python3"
}
